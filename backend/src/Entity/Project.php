<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['show_project', 'list_projects'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['show_project', 'list_projects'])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Event::class)]
    #[Groups(['show_project', 'list_projects'])]
    private Collection $events;

    #[ORM\ManyToOne(inversedBy: 'projects')]
    #[Groups(['show_project', 'list_projects'])]
    private ?ProjectField $field = null;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: ProjectNote::class)]
    #[Groups(['show_project', 'list_projects'])]
    private Collection $notes;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['show_project', 'list_projects'])]
    private $description = null;

    #[ORM\ManyToOne(inversedBy: 'projects')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ApiUser $user = null;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
            $event->setProject($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getProject() === $this) {
                $event->setProject(null);
            }
        }

        return $this;
    }

    public function getField(): ?ProjectField
    {
        return $this->field;
    }

    public function setField(?ProjectField $field): self
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return Collection<int, ProjectNote>
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(ProjectNote $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setProject($this);
        }

        return $this;
    }

    public function removeNote(ProjectNote $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getProject() === $this) {
                $note->setProject(null);
            }
        }

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?ApiUser
    {
        return $this->user;
    }

    public function setUser(?ApiUser $user): self
    {
        $this->user = $user;

        return $this;
    }
}
