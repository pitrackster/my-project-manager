<?php

namespace App\Entity;

use App\Repository\ProjectFieldRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectFieldRepository::class)]
class ProjectField
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user_settings'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['user_settings'])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'field', targetEntity: Project::class)]
    private Collection $projects;

    #[ORM\ManyToOne(inversedBy: 'projectFields')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ApiUser $user = null;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Project>
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
            $project->setField($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            // set the owning side to null (unless already changed)
            if ($project->getField() === $this) {
                $project->setField(null);
            }
        }

        return $this;
    }

    public function getUser(): ?ApiUser
    {
        return $this->user;
    }

    public function setUser(?ApiUser $user): self
    {
        $this->user = $user;

        return $this;
    }
}
