<?php

namespace App\Entity;

use App\Repository\AppThemeRepository;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AppThemeRepository::class)]
class Theme
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user_settings'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['user_settings'])]
    private ?bool $isDark = null;

    #[ORM\Column(length: 255)]
    #[Groups(['user_settings'])]
    private ?string $name = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('primary')]
    #[Groups(['user_settings'])]
    private ?string $primaryColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('secondary')]
    #[Groups(['user_settings'])]
    private ?string $secondaryColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('accent')]
    #[Groups(['user_settings'])]
    private ?string $accentColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('positive')]
    #[Groups(['user_settings'])]
    private ?string $positiveColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('negative')]
    #[Groups(['user_settings'])]
    private ?string $negativeColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('info')]
    #[Groups(['user_settings'])]
    private ?string $infoColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('warning')]
    #[Groups(['user_settings'])]
    private ?string $warningColor = null;

    #[ORM\Column(length: 7)]
    #[SerializedName('background')]
    #[Groups(['user_settings'])]
    private ?string $backgroundColor = null;

    #[ORM\ManyToOne(inversedBy: 'themes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ApiUser $user = null;

    #[ORM\Column]
    #[SerializedName('prefered')]
    #[Groups(['user_settings'])]
    private ?bool $isPrefered = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsDark(): ?bool
    {
        return $this->isDark;
    }

    public function setIsDark(bool $isDark): self
    {
        $this->isDark = $isDark;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrimaryColor(): ?string
    {
        return $this->primaryColor;
    }

    public function setPrimaryColor(string $primaryColor): self
    {
        $this->primaryColor = $primaryColor;

        return $this;
    }

    public function getSecondaryColor(): ?string
    {
        return $this->secondaryColor;
    }

    public function setSecondaryColor(string $secondaryColor): self
    {
        $this->secondaryColor = $secondaryColor;

        return $this;
    }

    public function getAccentColor(): ?string
    {
        return $this->accentColor;
    }

    public function setAccentColor(string $accentColor): self
    {
        $this->accentColor = $accentColor;

        return $this;
    }

    public function getPositiveColor(): ?string
    {
        return $this->positiveColor;
    }

    public function setPositiveColor(string $positiveColor): self
    {
        $this->positiveColor = $positiveColor;

        return $this;
    }

    public function getNegativeColor(): ?string
    {
        return $this->negativeColor;
    }

    public function setNegativeColor(string $negativeColor): self
    {
        $this->negativeColor = $negativeColor;

        return $this;
    }

    public function getInfoColor(): ?string
    {
        return $this->infoColor;
    }

    public function setInfoColor(string $infoColor): self
    {
        $this->infoColor = $infoColor;

        return $this;
    }

    public function getWarningColor(): ?string
    {
        return $this->warningColor;
    }

    public function setWarningColor(string $warningColor): self
    {
        $this->warningColor = $warningColor;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getUser(): ?ApiUser
    {
        return $this->user;
    }

    public function setUser(?ApiUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isIsPrefered(): ?bool
    {
        return $this->isPrefered;
    }

    public function setIsPrefered(bool $isPrefered): self
    {
        $this->isPrefered = $isPrefered;

        return $this;
    }
}
