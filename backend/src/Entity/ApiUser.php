<?php

namespace App\Entity;

use App\Repository\ApiUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: ApiUserRepository::class)]
class ApiUser implements UserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $uuid = null;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Project::class)]
    private Collection $projects;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Theme::class, orphanRemoval: true)]
    private Collection $themes;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: EventCategory::class, orphanRemoval: true)]
    private Collection $eventCategories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ProjectField::class, orphanRemoval: true)]
    private Collection $projectFields;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->themes = new ArrayCollection();
        $this->eventCategories = new ArrayCollection();
        $this->projectFields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->uuid;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Project>
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
            $project->setUser($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            // set the owning side to null (unless already changed)
            if ($project->getUser() === $this) {
                $project->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Theme>
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes->add($theme);
            $theme->setUser($this);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        if ($this->themes->removeElement($theme)) {
            // set the owning side to null (unless already changed)
            if ($theme->getUser() === $this) {
                $theme->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventCategory>
     */
    public function getEventCategories(): Collection
    {
        return $this->eventCategories;
    }

    public function addEventCategory(EventCategory $eventCategory): self
    {
        if (!$this->eventCategories->contains($eventCategory)) {
            $this->eventCategories->add($eventCategory);
            $eventCategory->setUser($this);
        }

        return $this;
    }

    public function removeEventCategory(EventCategory $eventCategory): self
    {
        if ($this->eventCategories->removeElement($eventCategory)) {
            // set the owning side to null (unless already changed)
            if ($eventCategory->getUser() === $this) {
                $eventCategory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProjectField>
     */
    public function getProjectFields(): Collection
    {
        return $this->projectFields;
    }

    public function addProjectField(ProjectField $projectField): self
    {
        if (!$this->projectFields->contains($projectField)) {
            $this->projectFields->add($projectField);
            $projectField->setUser($this);
        }

        return $this;
    }

    public function removeProjectField(ProjectField $projectField): self
    {
        if ($this->projectFields->removeElement($projectField)) {
            // set the owning side to null (unless already changed)
            if ($projectField->getUser() === $this) {
                $projectField->setUser(null);
            }
        }

        return $this;
    }
}
