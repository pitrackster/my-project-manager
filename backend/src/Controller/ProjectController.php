<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;

#[Route('/api')]
class ProjectController extends AbstractController
{
    #[Route('/project', name: 'project_list', methods: ['get'])]
    public function list(ProjectRepository $pr, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
        $projects = $pr->findByUser($user);
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('list_projects')
            ->toArray();

        $projectsJson = $serializer->serialize($projects, 'json', $context);
        return $this->json($projectsJson);
    }

    #[Route('/project/{id}', name: 'project_details', methods: ['get'], requirements: ['id' => '\d+'])]
    public function details(Project $project, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
        if (null === $project) {
            return $this->json("No project found with the given information.", JsonResponse::HTTP_NOT_FOUND);
        }

        $pUser = $project->getUser();
        if ($pUser->getUuid() !== $user->getUuid()) {
            return $this->json("No project found with the given information.", JsonResponse::HTTP_FORBIDDEN);
        }

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('show_project')
            ->toArray();

        $projectsJson = $serializer->serialize($project, 'json', $context);
        return $this->json($projectsJson);
    }

    #[Route('/project', name: 'project_delete', methods: ['delete'])]
    public function delete(Request $request, ProjectRepository $pr, ManagerRegistry $doctrine): JsonResponse
    {
        $user = $this->getUser();
        $data = json_decode($request->getContent(), true);
        if (!isset($data['id']) || empty($data['id'])) {
            return $this->json("No id provided.", JsonResponse::HTTP_BAD_REQUEST);
        }

        $project = $pr->findOneByIdAndUser($data['id'], $user);
        if (null === $project) {
            return $this->json("No project found with the given information.", JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $doctrine->getManager();
        $em->remove($project);
        $em->flush();

        return $this->json("ok");
    }

    #[Route('/project', name: 'project_create', methods: ['post'])]
    public function create(SerializerInterface $serializer, Request $request, ManagerRegistry $doctrine): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        if (isset($data['name']) && !empty($data['name'])) {
            $project = new Project();
            $user = $this->getUser();
            $project->setUser($user);
            $project->setName($data['name']);
            if (isset($data['description'])) {
                $project->setDescription($data['description']);
            }
            $entityManager = $doctrine->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            $context = (new ObjectNormalizerContextBuilder())
                ->withGroups('show_project')
                ->toArray();

            $projectJson = $serializer->serialize($project, 'json', $context);
            return $this->json($projectJson);
        }


        return $this->json('Please provide a name for the new Project.', JsonResponse::HTTP_BAD_REQUEST);
    }
}
