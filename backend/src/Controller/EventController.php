<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventCategoryRepository;
use App\Repository\EventRepository;
use App\Repository\ProjectRepository;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api')]
class EventController extends AbstractController
{
    #[Route('/events-of-the-day', name: 'events_of_the_day', methods: ['get'])]
    public function getEventsOfTheDay(EventRepository $er, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
        $events = $er->todaysEventForUser($user);

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('list_event')
            ->toArray();

        $eventsJson = $serializer->serialize($events, 'json', $context);

        return $this->json($eventsJson);
    }

    #[Route('/event', name: 'event_list', methods: ['get'])]
    public function list(EventRepository $er): JsonResponse
    {
        $user = $this->getUser();
        $events = $er->findByUser($user);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/EventController.php',
        ]);
    }

    #[Route('/event', name: 'event_create', methods: ['post'])]
    public function create(Request $request, ManagerRegistry $doctrine, EventCategoryRepository $ecr, ProjectRepository $pr, SerializerInterface $serializer): JsonResponse
    {
        
        $user = $this->getUser();
        $data = json_decode($request->getContent(), true);
        if (isset($data["pid"]) && !empty($data["pid"]) && isset($data["date"]) && !empty($data["date"]) && isset($data["tid"]) && !empty($data["tid"]) && isset($data["time"]) && !empty($data["time"])) {
            $project = $pr->findOneBy(["id" => $data["pid"]]);
            if (null === $project || $project->getUser() !== $user) {
                return $this->json('Event project not found with the given data.', JsonResponse::HTTP_BAD_REQUEST);
            }
            $eventCategory = $ecr->findOneBy(["id" => $data["tid"]]);
            if (null === $eventCategory || $eventCategory->getUser() !== $user) {
                return $this->json('Event category not found with the given data.', JsonResponse::HTTP_BAD_REQUEST);
            }

            $event = new Event();
            $event->setProject($project);
            $event->setCategory($eventCategory);
            $event->setTime($data["time"]);
            $event->setDate(new DateTime($data["date"]));
            if (isset($data["comments"]) && !empty($data["comments"])) {
                $event->setComments($data["comments"]);
            }

            $em = $doctrine->getManager();
            $em->persist($event);
            $em->flush();

            $context = (new ObjectNormalizerContextBuilder())
                ->withGroups('show_event')
                ->toArray();

            $eventJson = $serializer->serialize($event, 'json', $context);
            return $this->json($eventJson);
        } else {
            return $this->json('Missing data.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
