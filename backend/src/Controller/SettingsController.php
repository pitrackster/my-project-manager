<?php

namespace App\Controller;

use App\Entity\ApiUser;
use App\Entity\EventCategory;
use App\Entity\Project;
use App\Repository\EventCategoryRepository;
use App\Repository\ProjectRepository;
use App\Repository\ThemeRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;

#[Route('/api')]
class SettingsController extends AbstractController
{
    #[Route('/settings', name: 'user_settings', methods: ['get'])]
    public function get(SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();

        // find settings for user (themes, prefered theme, event categories and project fields)
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('user_settings')
            ->toArray();

        $themes = $user->getThemes();
        $eventCategories = $user->getEventCategories();
        $projectFields = $user->getProjectFields();

        $themesJson = $serializer->serialize($themes, 'json', $context);
        $eventCategoriesJson = $serializer->serialize($eventCategories, 'json', $context);
        $projectFieldsJson = $serializer->serialize($projectFields, 'json', $context);

        return $this->json([

            'themes' => $themesJson,
            'categories' =>  $eventCategoriesJson,
            'fields' => $projectFieldsJson
        ]);
    }

    #[Route('/category', name: 'user_categories', methods: ['get'])]
    public function getCategories(SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();

        // find settings for user (themes, prefered theme, event categories and project fields)
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('user_settings')
            ->toArray();

        $eventCategories = $user->getEventCategories();
        $eventCategoriesJson = $serializer->serialize($eventCategories, 'json', $context);

        return $this->json($eventCategoriesJson);
    }

    /**
     * Create an EventCategory
     */
    #[Route('/category', name: 'create_category', methods: ['post'])]
    public function createEventCategory(Request $request, ManagerRegistry $doctrine, SerializerInterface $serializer): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $name = isset($data["name"]) && !empty($data["name"]) ? $data["name"] : false;
        if (!$name) {
            return new JsonResponse('Category name can not be empty.', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $eventCategory = new EventCategory();
        $eventCategory->setUser($user);
        $eventCategory->setName($name);

        $entityManager = $doctrine->getManager();
        $entityManager->persist($eventCategory);
        $entityManager->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('user_settings')
            ->toArray();

        $categoryJson = $serializer->serialize($eventCategory, 'json', $context);

        return $this->json([
            $categoryJson,
        ]);
    }

    /**
     * Edit an EventCategory
     */
    #[Route('/category', name: 'edit_category', methods: ['patch'])]
    public function editEventCategory(Request $request, EventCategoryRepository $ecr, ManagerRegistry $doctrine, SerializerInterface $serializer): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $id = isset($data["id"]) && !empty($data["id"]) ? $data["id"] : false;
        if (!$id) {
            return new JsonResponse('Please provide a valid id.', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $category = $ecr->findOneByIdAndUser($id, $user);
        if (null === $category) {
            return new JsonResponse('No category found with the given data.', Response::HTTP_BAD_REQUEST);
        }

        $newName = isset($data["name"]) && !empty($data["name"]) ? $data["name"] : false;
        if (!$newName) {
            return new JsonResponse('Category name can not be empty.', Response::HTTP_BAD_REQUEST);
        }

        $category->setName($newName);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($category);
        $entityManager->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('user_settings')
            ->toArray();

        $categoryJson = $serializer->serialize($category, 'json', $context);

        return $this->json([
            'data' => $categoryJson,
        ]);
    }

    /**
     * delete an EventCategory
     */
    #[Route('/category', name: 'delete_category', methods: ['delete'])]
    public function deleteEventCategory(Request $request, EventCategoryRepository $ecr, ManagerRegistry $doctrine): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $id = isset($data["id"]) && !empty($data["id"]) ? $data["id"] : false;
        if (!$id) {
            return new JsonResponse('Please provide a valid id.', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $category = $ecr->findOneByIdAndUser($id, $user);
        if (null === $category) {
            return new JsonResponse('No category found with the given data.', Response::HTTP_BAD_REQUEST);
        }

        $entityManager = $doctrine->getManager();
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->json([
            'message' => 'deleted',
        ]);
    }

    #[Route('/prefered-theme', name: 'prefered_theme', methods: ['get'])]
    public function getPreferedTheme(Request $request, ThemeRepository $tr, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
        $theme = $tr->findPreferedThemeForUser($user);
        if (null === $theme) {
            return new JsonResponse('No theme found with the given data.', Response::HTTP_BAD_REQUEST);
        }

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('user_settings')
            ->toArray();

        $themeJson = $serializer->serialize($theme, 'json', $context);
        return $this->json(
            $themeJson
        );
    }

    #[Route('/theme', name: 'set_prefered_theme', methods: ['patch'])]
    public function setPreferedTheme(Request $request, ThemeRepository $tr, ManagerRegistry $doctrine): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $id = isset($data["id"]) && !empty($data["id"]) ? $data["id"] : false;
        if (!$id) {
            return new JsonResponse('Please provide a valid id.', Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        $theme = $tr->findOneByIdAndUser($id, $user);
        if (null === $theme) {
            return new JsonResponse('No theme found with the given data.', Response::HTTP_BAD_REQUEST);
        }

        $themes = $user->getThemes();
        $em = $doctrine->getManager();

        foreach ($themes as $theme) {
            if ($theme->getId() !== $id) {
                $theme->setIsPrefered(false);
            } else {
                $theme->setIsPrefered(true);
            }
            $em->persist($theme);
        }
        $em->flush();

        return $this->json([
            'message' => 'ok',
        ]);
    }


    #[Route('/field', name: 'create_field', methods: ['post'])]
    public function createProjectField(Request $request, ManagerRegistry $doctrine): JsonResponse
    {
        $data = json_decode($request->getContent(), true);


        return $this->json([
            'data' => '',
        ]);
    }
}
