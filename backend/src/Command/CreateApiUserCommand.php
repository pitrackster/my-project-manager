<?php

namespace App\Command;

use App\Entity\ApiUser;
use App\Entity\EventCategory;
use App\Entity\ProjectField;
use App\Entity\Theme;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command is what users type after "php bin/console"
#[AsCommand(
    name: 'app:create-user',
    description: 'Creates a new user.',
    hidden: false,
    aliases: ['app:add-user']
)]
class CreateApiUserCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new ApiUser();
        $uuid = Uuid::v4();
        $user->setUuid($uuid);
        $output->writeln('');
        $output->writeln('');
        $output->writeln('=================');
        $output->writeln('= Creating user =');
        $output->writeln('=================');
        $output->writeln('');
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln('=====================================================');
        $output->writeln('=> \o/ User created, let\'s create a set of settings =');
        $output->writeln('=====================================================');
        $output->writeln('');
        $output->writeln('===========================================================');
        $output->writeln('= A default set of settings will be created for this user =');
        $output->writeln('===========================================================');
        $output->writeln('');
        $this->addDefaultSettings($user);
        $output->writeln('==============================================================');
        $output->writeln('=> (^_^) A default set of settings are created for this user =');
        $output->writeln('==============================================================');
        $output->writeln('');

        $output->writeln('===============================================================================================');
        $output->writeln('=> (\\);;;°-°;;;(/) User successfully generated with uuid ' . $uuid . ' =');
        $output->writeln('===============================================================================================');
        $output->writeln('');
        return Command::SUCCESS;
    }

    private function addDefaultSettings(ApiUser $user) 
    {
        $categories = ["Développement", "Maintenance", "Support", "réunion", "AMOA"];
        $fields = ["Scolarité", "Finance", "RH"];
        $themesString = '[
            {
              "isDark": false,
              "name": "Quasar",
              "primary": "#1976d2",
              "secondary": "#26a69a",
              "accent": "#9c27b0",
              "positive": "#21ba45",
              "negative": "#c10015",
              "info": "#31ccec",
              "warning": "#f2c037",
              "background": "#fff"
            },
            {
              "isDark": true,
              "name": "Quasar Dark",
              "primary": "#6d6d6d",
              "secondary": "#5d5d5d",
              "accent": "#4d4d4d",
              "positive": "#20615b",
              "negative": "#a21232",
              "info": "#3e35a8",
              "warning": "#c1a54d",
              "background": "#2c2c2c"
            },
            {
              "isDark": true,
              "name": "Synthwave",
              "primary": "#ff7edb",
              "secondary": "#93ce8f",
              "accent": "#9c27b0",
              "positive": "#20615b",
              "negative": "#a21232",
              "info": "#3e35a8",
              "warning": "#c1a54d",
              "background": "#262335"
            }
          ]';

        $themes = json_decode($themesString, true);

        foreach ($themes as $index => $theme) {
            $entity = new Theme();
            $entity->setUser($user);
            $entity->setIsPrefered($index === 0);
            $entity->setIsDark($theme["isDark"]);
            $entity->setName($theme["name"]);
            $entity->setPrimaryColor($theme["primary"]);
            $entity->setSecondaryColor($theme["secondary"]);
            $entity->setAccentColor($theme["accent"]);
            $entity->setPositiveColor($theme["positive"]);
            $entity->setNegativeColor($theme["negative"]);
            $entity->setInfoColor($theme["info"]);
            $entity->setWarningColor($theme["warning"]);
            $entity->setBackgroundColor($theme["background"]);
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();

        foreach ($categories as $label) {
            $entity = new EventCategory();
            $entity->setName($label);
            $entity->setUser($user);
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();

        foreach ($fields as $label) {
            $entity = new ProjectField();
            $entity->setName($label);
            $entity->setUser($user);
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }
}
