TASK MANAGER FRONTEND & BACKEND
===============================


- PHP8.1
- COMPOSER
- NODEJS
- QuasarCli
- MySQL / MariaDB

## BACKEND

- `cd backend`
- `composer install --prefer-source`
- `cp .env .env.local` and set proper values
- `php bin/console doctrine:schema:update --force` or `make migrate`
- you should create an api user `php bin/console app:create-user` or `make user`
- `symfony server:start`


## FRONTEND

- `yarn install`
- `quasar dev`


## quasar store
https://quasar.dev/quasar-cli-vite/state-management-with-pinia#introduction

## symfony serialization

- circular dependencies https://symfony.com/doc/current/components/serializer.html#handling-circular-references