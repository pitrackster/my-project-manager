/**
 * Create a CSV string from Js Object
 * @param {Array} rows
 * @param {Object} header
 * @param {String} separator
 * @returns
 */
export const createCsvString = (rows, header, separator) => {
  if (!rows || !rows.length) {
    return;
  }

  let keys = [];

  if (header) {
    rows.unshift(header);
    keys = Object.keys(header);
  } else {
    keys = Object.keys(rows[0]);
  }

  const content = rows
    .map((row) => {
      return keys
        .map((k) => {
          let cell = row[k] === null || row[k] === undefined ? "" : row[k];
          cell = cell.toString().replace(/"/g, '""');
          if (cell.search(/("|,|\n)/g) >= 0) {
            cell = `"${cell}"`;
          }
          return cell;
        })
        .join(separator);
    })
    .join("\n");
  return content;
};
