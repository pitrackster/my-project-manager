// convert a string DD/MM/YYYY to a date object
export const dateFromFrenchDateString = (dateString) => {
  const dateArray = dateString.split("/");
  const date = Date.parse(`${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`);
  return date;
};
