import { createI18n } from "vue-i18n";
import messages from "src/i18n";

import { Quasar } from "quasar";

// inside of a Vue file
import { useQuasar } from "quasar";

export default ({ app }) => {
  const $q = useQuasar();
  const locale = Quasar.lang.getLocale(); // returns a string
  // console.log("locale", locale);
  // Create I18n instance
  const i18n = createI18n({
    locale: locale,
    messages,
  });

  Quasar.lang.set(locale);

  // Tell app to use the I18n instance
  app.use(i18n);
};
