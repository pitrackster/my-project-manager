
import { Quasar } from "quasar";

export default async () => {
  const langIso = Quasar.lang.getLocale(); //"de"; // ... some logic to determine it (use Cookies Plugin?)

  try {
    await import(
      /* webpackInclude: /(fr|en-US)\.js$/ */
      "quasar/lang/" + langIso
    ).then((lang) => {
      // console.log(lang.default);
      Quasar.lang.set(lang.default);
    });
  } catch (err) {
    // Requested Quasar Language Pack does not exist,
    // let's not break the app, so catching error
    //Quasar.lang.set(Quasar.lang.)
  }
};
